
object roque {
	var avesQueEntrena = []
	
	method agregarParaEntrenar(unAve) { avesQueEntrena.add(unAve) }
	method dejarDeEntrenar(unAve) { avesQueEntrena.remove(unAve) }
	method entrenar() { avesQueEntrena.forEach({ ave => self.entrenarAve(ave) }) }
	method entrenarAve(ave) {
		ave.volar(10)
		ave.comer(300)
		ave.volar(5)
		ave.haceLoQueQuieras()
	}
}