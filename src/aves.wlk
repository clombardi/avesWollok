class Ave {
	method valor() = self.energia() + self.experiencia()
	
	method energia() = 20
	
	method experiencia() = 40
}

class Golondrina inherits Ave {
	var energia = 0
	
	method comer(gramos) { energia += gramos * 4 }
	method volar(kms) { energia -= self.energiaNecesariaParaVolar(kms) }
	override method energia() { return energia }
	method energiaDisponible() { return self.energia() }
	
	method puedeVolar(kms) { 
		return self.energiaDisponible() >= self.energiaNecesariaParaVolar(kms)
	}
	method energiaNecesariaParaVolar(kms) { return kms + 10 }
	
	override method experiencia() {
		// TODO la mitad de la distancia que volo 
		return 0
	}
	
	method estaDebil() { return self.energia() < 50 }
	method estaFeliz() { return self.energia() >= 500 && self.energia() <= 1000 }
	method cuantoQuiereVolar() { 
		var cuanto = self.energia() / 5
		if (self.energia() >= 300 && self.energia() <= 400) {
			cuanto += 10
		}
		if (self.energia() % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}
	
	method haceLoQueQuieras() {
		// esto no esta bien, podria hacer que coma y despues vuele 
//		if (self.estaDebil()) {
//			self.comer(20)
//		} 
//		if (self.estaFeliz()) {
//			self.volar(self.cuantoQuiereVolar())
//		}

		// este esta bien, hace una cosa o la otra, nunca las dos
		if (self.estaDebil()) {
			self.comer(20)
		} else if (self.estaFeliz()) {
			self.volar(self.cuantoQuiereVolar())
		}
	}
	
}

class GolondrinaAndina inherits Golondrina {
	override method estaFeliz() {
		// TODO preguntar como era esto
		return false
	}
}


class GolondrinaPrecavida inherits Golondrina {
	override method energiaDisponible() { return super() - 25 }
}


class GolondrinaPensativa inherits Golondrina {
	// TODO actualizar esta variable
	var minutosQuePenso
	
	override method experiencia() {
		return super() + minutosQuePenso * 3 
	}
	
	override method estaFeliz() {
		return minutosQuePenso >= 15
	}
}


class GolondrinaZen inherits GolondrinaPensativa {
	// TODO actualizar esta variable
	var vecesQueEntroAlNirvana
	
	override method experiencia() {
		return super() + 5 * vecesQueEntroAlNirvana
	}	
}


class GolondrinaZenAfricana inherits GolondrinaZen {
	override method volar(kms) {
		if (kms % 4 != 1) { super(kms) }
	}
}

class Gorrion inherits Ave {
	var energia = 0
	
	method comer(gramos) { energia += gramos * 2 }
	method volar(kms) { energia -= self.energiaNecesariaParaVolar(kms) }
	override method energia() { return energia }
	
	method puedeVolar(kms) { return energia >= self.energiaNecesariaParaVolar(kms) }
	method energiaNecesariaParaVolar(kms) { return (kms / 2) + 1 }
	
	method haceLoQueQuieras() { self.volar(1) }
	
	override method experiencia() { return self.energia() % 20 }		
}


class Paloma {
	var gramosIngeridos = 0
	var kmsRecorridos = 0
	
	method comer(gramos) { gramosIngeridos += gramos }
	method volar(kms) { kmsRecorridos += kms }
	method gramosIngeridos() { return gramosIngeridos }
	method kmsRecorridos() { return kmsRecorridos }

	method puedeVolar(kms) { return true }
	
	method haceLoQueQuieras() { /* no hace nada */}		
}















